<?php
/*
Plugin Name: KathrynFreemanWeb-hSlider
Plugin URI: 
Description: A plugin to show latest news and books for KathrynFreeman.co.uk
Version: 0.1
Author: James Harding
Author URI: http://jamesharding.me
License: GPL
*/

	add_action('admin_menu', 'hSlider_admin_add_page');
		function hSlider_admin_add_page() {
		add_options_page('Homepage Slider Options', 'Homepage Slider Options', 'manage_options', 'hSlider', 'hSlider_options_page');
	}

	function hSlider_options_page() {
		echo "<div class='wrap'>" . 
		"<h2>Homepage Slider Options</h2>" .
		"<form action='options.php' method='post'>";
		settings_fields('hSlider_options');
		do_settings_sections('hSlider');
		
		submit_button();
		echo "</form></div>";
 	}

	add_action('admin_init', 'hSlider_admin_init');
	function hSlider_admin_init(){
		register_setting( 'hSlider_options', 'hSlider_pageID');
		register_setting( 'hSlider_options', 'hSlider_aToken');
		add_settings_section('hSlider_main', 'Feed Settings', 'hSlider_section_text', 'hSlider');
		add_settings_field('hSlider_pageID', 'Facebook Page ID', 'hSlider_setting_pageID', 'hSlider', 'hSlider_main');
		add_settings_field('hSlider_aToken', 'Access Token', 'hSlider_setting_aToken', 'hSlider', 'hSlider_main');
	}

	function hSlider_section_text() {
		echo '<p>Main description of this section here.</p>';
	}

	function hSlider_setting_pageID() {
		$options = get_option('hSlider_pageID');
		echo "<input id='hSlider_text_pageID' name='hSlider_pageID' type='text' value='" . get_option("hSlider_pageID") . "' />";
	}

	function hSlider_setting_aToken() {
		$options = get_option('hSlider_aToken');
		echo "<input id='hSlider_text_aToken' name='hSlider_aToken' type='text' value='" . get_option("hSlider_aToken") . "' />";
	}

	function makeFbNewsFeed() {
		//add_option(fbPageID, )
		echo "<link rel='stylesheet' type='text/css' href='" . plugins_url( 'style.css', __FILE__ ) . "'>";
?>